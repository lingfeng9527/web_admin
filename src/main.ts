import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router";
import pinia from "./store/store";
import "@/static/css/reset.css";
import "@/static/js/iconfont.js";
import 'animate.css/animate.min.css'
import locale from "element-plus/lib/locale/lang/zh-cn";
import ElementPlus from "element-plus";
createApp(App).use(pinia).use(router).use(ElementPlus, { locale }).mount("#app");
