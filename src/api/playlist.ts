import api from '@/utils/axios/api'
// 获取精品歌单
export const topPlayListHighQuality=(params:object)=>api.get('/api/top/playlist/highquality',{params})
// 精品歌单标签列表
export const topPlayListHighQualityTags=(params:object)=>api.get('/api/playlist/highquality/tags',{params})
// 所有榜单内容摘要
export const toplistDetail=(params:object)=>api.get('/api/toplist/detail',{params})
// 歌手分类列表
export const artistList=(params:object)=>api.get('/api/artist/list',{params})
// 获取歌手详情
export const artistDetail=(params:object)=>api.get('/api/artist/detail',{params})
// 获取歌手详情
export const artists=(params:object)=>api.get('/api/artists',{params})
// 新碟上架
export const albumNew=(params:object)=>api.get('/api/album/new',{params})
// 所有榜单
export const toplist=()=>api.get('/api/toplist')
// 获取歌单详情
export const playlistDetail=(params:object)=>api.get('/api/playlist/detail',{params})
// 获取歌单所有歌曲
export const playlistTrackAll=(params:object)=>api.get('/api/playlist/track/all',{params})
// 歌单详情动态
export const playlistDetailDynamic=(params:object)=>api.get('/api/playlist/detail/dynamic',{params})
// 获取音乐 url - 新版
export const songUrlV1=(params:object)=>api.get('/api/song/url/v1',{params})
// 音乐是否可用
export const checkMusic=(params:object)=>api.get('/api/check/music',{params})
// 歌单分类
export const playlistCatlist=()=>api.get('/api/playlist/catlist',)
// 电台 - 分类
export const djBanner=()=>api.get('/api/dj/banner',)
// 电台 - 分类
export const djCatelist=()=>api.get('/api/dj/catelist',)
// 电台 - 推荐
export const djRecommend=()=>api.get('/api/dj/recommend',)