import { defineStore } from "pinia";
let navOptionsRouterList:any = []
if (sessionStorage.navOptionsRouterList) {
  navOptionsRouterList = JSON.parse(sessionStorage.navOptionsRouterList) || [];
}
const piniaStore: any = defineStore("main", {
  state: () => {
    return {
      // 左侧菜单数据
      routerUrlList: [],
      // 面包屑导航
      crumbsStr: "分析页",
      // 顶部导航菜单数据
      navOptionsRouterList: navOptionsRouterList,
      // navOptionsRouterList: <any>[
      // {
      //     id: 2,
      //     routerUrl: "/dashboard/analysis",
      //     title: "分析页",
      //     icon: "#icon-pc",
      //     isActive: true,
      // },
      // ],
    };
  },
  // 计算属性
  getters: {},
  // 异步操作
  actions: {
    // 左侧菜单修改
    setMenu(data: any) {
      this.routerUrlList = data;
    },
    // 面包屑修改
    setCrumbs(data: any) {
      this.crumbsStr = data;
    },
    // 点击菜单，加入顶部选项栏
    setNavOptions(data: any) {
      if (this.navOptionsRouterList.length > 0) {
        var indexNum = this.navOptionsRouterList.findIndex(
          (item: any) => item.id === data.id
        );
        if (indexNum == -1) {
          this.navOptionsRouterList.forEach((i: any) => {
            i.isActive = false;
          });
          if (this.navOptionsRouterList.length >= 10) {
            this.navOptionsRouterList.splice(1, 1);
          }
          this.navOptionsRouterList.push(data);
        } else {
          this.navOptionsRouterList.forEach((i: any) => {
            i.isActive = false;
            if (i.id == data.id) {
              i.isActive = true;
            }
          });
        }
      } else {
        this.navOptionsRouterList.push(data);
      }
    },
  },
});

export default piniaStore;
