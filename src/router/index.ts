import { createRouter, createWebHistory } from "vue-router";
import routes from "./router";

// 导出路由
const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to: any, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title;
    }
    let token = localStorage.getItem("token") || "";
    if (token) {
        next();
    } else {
        if (to.path === "/login") {
            next();
        } else {
            next({ path: "/login" });
        }
    }
});

export default router;
