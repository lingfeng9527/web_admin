import pinia from "../store/store";
import piniaStore from "@/store";
const store = piniaStore(pinia);
let modules = import.meta.glob("../views/**/*.vue");
let sysBaseTitle: string = "后台管理系统";

// 路由列表
let routes: any = [
    {
        path: "/login",
        name: "home",
        component: () => import("@/views/login/login.vue"),
        meta: {
            title: sysBaseTitle + "-登录",
        },
    },
    {
        path: "/",
        name: "index",
        component: () => import("@/views/layout.vue"),
        meta: {
            title: sysBaseTitle,
        },
        children: [],
    },
];

let menuAllListRes = [
    {
        id: 1,
        // index: "1",
        routerUrl: "/dashboard",
        title: "仪表盘",
        icon: "#icon-pc",
        path: "/dashboard",
        name: "dashboard",
        component: "views/dashboard/index.vue",
        isHide: 2,
        parentId: 0,
        sort: 1,
        children: [
            {
                id: 2,
                // index: "1-2",
                routerUrl: "/dashboard/analysis",
                title: "分析页",
                icon: "#icon-rili2",
                path: "analysis",
                name: "analysis",
                component: "views/dashboard/analysis/analysis.vue",
                isHide: 2,
                parentId: 1,
                sort: 1,
            },
            {
                id: 3,
                // index: "1-3",
                routerUrl: "/dashboard/workbench",
                title: "工作台",
                icon: "#icon-diannao",
                path: "workbench",
                name: "workbench",
                component: "views/dashboard/workbench/workbench.vue",
                isHide: 2,
                parentId: 1,
                sort: 1,
            },
        ],
    },
    {
        id: 5,
        // index: "5",
        routerUrl: "/system",
        title: "系统管理",
        icon: "#icon-xt8",
        path: "/system",
        name: "system",
        component: "views/system/index.vue",
        isHide: 2,
        parentId: 0,
        sort: 1,
        children: [
            {
                id: 6,
                // index: "5-6",
                routerUrl: "/system/user",
                title: "用户列表",
                icon: "#icon-jiaosequnti",
                path: "user",
                name: "user",
                component: "views/system/user/user.vue",
                isHide: 2,
                parentId: 5,
                sort: 1,
            },
            {
                id: 7,
                // index: "5-7",
                routerUrl: "/system/role",
                title: "角色管理",
                icon: "#icon-diannao",
                path: "role",
                name: "role",
                component: "views/system/role/role.vue",
                isHide: 2,
                parentId: 5,
                sort: 1,
            },
            {
                id: 8,
                // index: "5-8",
                routerUrl: "/system/menu",
                title: "菜单管理",
                icon: "#icon-yanzhengma3",
                path: "menu",
                name: "menu",
                component: "views/system/menu/menu.vue",
                isHide: 2,
                parentId: 5,
                sort: 1,
            },
        ],
    },
    {
        id: 9,
        routerUrl: "/article",
        title: "文章管理",
        icon: "#icon-danpin",
        path: "/article",
        name: "article",
        component: "views/article/index.vue",
        isHide: 2,
        parentId: 0,
        sort: 1,
        children: [
            {
                id: 10,
                routerUrl: "/article/list",
                title: "文章分类",
                icon: "#icon-liebiaoxingshi",
                path: "list",
                name: "list",
                component: "views/article/list/list.vue",
                isHide: 2,
                parentId: 5,
                sort: 1,
            },
            {
                id: 11,
                routerUrl: "/article/classificati",
                title: "文章列表",
                icon: "#icon-xinzengdaohangliebiao",
                path: "classificati",
                name: "classificati",
                component: "views/article/list/classificati.vue",
                isHide: 2,
                parentId: 5,
                sort: 1,
            },
        ],
    },
];

// 无限极菜单
// 根据返回的接口，重组菜单数据和路由数据
let menusRes: any = [];
let routesRes: any = [];
menuAllListRes.forEach((item) => {
    let menusResItem: any = {
        icon: item.icon,
        id: item.id,
        // index: item.index,
        routerUrl: item.routerUrl,
        title: item.title,
    };
    let routesResItem: any = {
        path: item.path,
        name: item.name,
        meta: {
            title: item.title + "-" + sysBaseTitle,
        },
        component: modules[`../${item.component}`],
    };
    if (item.children) {
        menusResItem.children = [];
        routesResItem.children = [];
        menuRecursiveFilter(item.children, menusResItem);
        routerRecursiveFilter(item.children, routesResItem);
    }
    menusRes.push(menusResItem);
    routesRes.push(routesResItem);
});

// 菜单数据递归
function menuRecursiveFilter(children: any, menusResItem: any) {
    children.forEach((item: any) => {
        let m: any = {
            icon: item.icon,
            id: item.id,
            index: item.index,
            routerUrl: item.routerUrl,
            title: item.title,
        };
        if (item.children) {
            m.children = [];
            menuRecursiveFilter(item.children, m);
        }
        menusResItem.children.push(m);
    });
}

// 路由数据递归
function routerRecursiveFilter(children: any, routesResItem: any) {
    children.forEach((item: any) => {
        let r: any = {
            path: item.path,
            name: item.name,
            meta: {
                title: item.title + "-" + sysBaseTitle,
            },
            component: modules[`../${item.component}`],
        };
        if (item.children) {
            r.children = [];
            routerRecursiveFilter(item.children, r);
        }
        routesResItem.children.push(r);
    });
}

routes[1].children.push(...routesRes);
store.setMenu(menusRes);
export default routes;
