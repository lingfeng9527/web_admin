/// <reference types="vite/client" />

declare module "*.vue" {
    import type { DefineComponent } from "vue";
    const component: DefineComponent<{}, {}, any>;
    export default component;
}
export { }
declare global {
    const ElMessage: typeof import('element-plus/es')['ElMessage']
    const ElMessageBox: typeof import('element-plus/es')['ElMessageBox']
    const ElLoading: typeof import('element-plus')['ElLoading']
}
declare module "path" {
    import type { path } from "path";
}

interface ImportMetaEnv {
    readonly VITE_BASE_URL: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}
